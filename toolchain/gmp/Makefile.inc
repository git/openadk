# This file is part of the OpenADK project. OpenADK is copyrighted
# material, please see the LICENCE file in the top-level directory.

PKG_NAME:=		gmp
PKG_VERSION:=		6.3.0
PKG_RELEASE:=		1
PKG_HASH:=		a3c2b80201b89e68616f4ad30bc66aee4927c3ce50e33929ca819d5c43538898
PKG_SITES:=		${MASTER_SITE_GNU:=gmp/}
