# This file is part of the OpenADK project. OpenADK is copyrighted
# material, please see the LICENCE file in the top-level directory.

PKG_NAME:=		binutils
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_44),y)
PKG_VERSION:=		2.44
PKG_RELEASE:=		1
PKG_HASH:=		0cdd76777a0dfd3dd3a63f215f030208ddb91c2361d2bcc02acec0f1c16b6a2e
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_43_1),y)
PKG_VERSION:=		2.43.1
PKG_RELEASE:=		1
PKG_HASH:=		e4c38b893f590853fbe276a6b8a1268101e35e61849a07f6ee97b5ecc97fbff8
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_42),y)
PKG_VERSION:=		2.42
PKG_RELEASE:=		1
PKG_HASH:=		5d2a6c1d49686a557869caae08b6c2e83699775efd27505e01b2f4db1a024ffc
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_41),y)
PKG_VERSION:=		2.41
PKG_RELEASE:=		1
PKG_HASH:=		48d00a8dc73aa7d2394a7dc069b96191d95e8de8f0da6dc91da5cce655c20e45
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_40),y)
PKG_VERSION:=		2.40
PKG_RELEASE:=		1
PKG_HASH:=		d7f82c4047decf43a6f769ac32456a92ddb6932409a585c633cdd4e9df23d956
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_39),y)
PKG_VERSION:=		2.39
PKG_RELEASE:=		1
PKG_HASH:=		d12ea6f239f1ffe3533ea11ad6e224ffcb89eb5d01bbea589e9158780fa11f10
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_38),y)
PKG_VERSION:=		2.38
PKG_RELEASE:=		1
PKG_HASH:=		b3f1dc5b17e75328f19bd88250bee2ef9f91fc8cbb7bd48bdb31390338636052
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_2_37),y)
PKG_VERSION:=		2.37
PKG_RELEASE:=		1
PKG_HASH:=		c44968b97cd86499efbc4b4ab7d98471f673e5414c554ef54afa930062dbbfcb
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_KVX),y)
PKG_VERSION:=		782547a4e2bdf1308728032853678ca69bb154ea
PKG_GIT:=		hash
PKG_RELEASE:=		1
PKG_SITES:=		https://github.com/kalray/gdb-binutils.git
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_ARC),y)
PKG_VERSION:=		arc-2023.09
PKG_GIT:=		tag
PKG_RELEASE:=		1
PKG_SITES:=		https://github.com/foss-for-synopsys-dwc-arc-processors/binutils-gdb.git
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_AVR32),y)
PKG_VERSION:=		2.20.1
PKG_RELEASE:=		1
PKG_HASH:=		71d37c96451333c5c0b84b170169fdcb138bbb27397dc06281905d9717c8ed64
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.bz2
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_NDS32),y)
PKG_VERSION:=		2.24
PKG_RELEASE:=		1
PKG_HASH:=		e5e8c5be9664e7f7f96e0d09919110ab5ad597794f5b1809871177a0f0f14137
PKG_SITES:=		${MASTER_SITE_GNU:=binutils/}
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.bz2
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_XTENSA),y)
PKG_VERSION:=		xtensa-fdpic-abi-spec-1.4
PKG_GIT:=		tag
PKG_RELEASE:=		1
PKG_SITES:=		https://github.com/jcmvbkbc/binutils-gdb-xtensa.git
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
ifeq ($(ADK_TOOLCHAIN_BINUTILS_GIT),y)
PKG_VERSION:=		git
PKG_RELEASE:=		1
PKG_SITES:=		git://sourceware.org/git/binutils-gdb.git
DISTFILES:=		${PKG_NAME}-${PKG_VERSION}.tar.gz
endif
