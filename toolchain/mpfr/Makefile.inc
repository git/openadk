# This file is part of the OpenADK project. OpenADK is copyrighted
# material, please see the LICENCE file in the top-level directory.

PKG_NAME:=		mpfr
PKG_VERSION:=		4.2.1
PKG_RELEASE:=		1
PKG_HASH:=		277807353a6726978996945af13e52829e3abd7a9a5b7fb2793894e18f1fcbb2
PKG_SITES:=		http://www.mpfr.org/mpfr-current/
